import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  password;
  email;

  constructor( //Injeção de dependências.
    private builder: FormBuilder, //----> construção do formgroup.
    private service: LoginService,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.isUserLoggedIn();

    this.loginForm = this.builder.group({
      email: ['', [Validators.email, Validators.required]], //verifica se é email e preenchimento obrigatório.
      password: ['', [Validators.required, Validators.minLength(8)]] //preenchimento obrigatório e tamanho mínimo.
    });
  }

  isUserLoggedIn() {
    this.service.isLoggedIn.subscribe(user => {
            if(user){
              // usuário logado.
              this.nav.navigateForward('home');
            }
        }
    );
  }

  login() { //não trata da lógica de apresentação da página.
    const user = this.loginForm.value;
    this.service.login(user);
  }

}
