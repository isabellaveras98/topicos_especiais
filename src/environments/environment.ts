// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDVyK7wf51nAR4N3auGIv46uBm6kehBt9k",
    authDomain: "controleisaveras.firebaseapp.com",
    projectId: "controleisaveras",
    storageBucket: "controleisaveras.appspot.com",
    messagingSenderId: "669002041003",
    appId: "1:669002041003:web:2de472ad9b58c04cec99ee",
    measurementId: "G-H60HM5RK9E"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
